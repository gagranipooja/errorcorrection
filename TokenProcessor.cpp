#include "globals.h"
#include "TokenProcessor.h"
#include "HuffmanCoding.h"
#include<math.h>
#include<iostream>
#include<fstream>
#define HASH_BUCKET_SIZE 1021

using namespace std;

matrixMap TokenProcessor::tmMap;
matrixMap TokenProcessor::emMap;
matrixMap TokenProcessor::smMap;

TokenProcessor::TrieNode * TokenProcessor::root = NULL;
Json::Value TokenProcessor::jsonRoot; 
tagVec TokenProcessor::masterTagsList;
bool TokenProcessor::isInitialized = false;

TokenProcessor::TokenProcessor(){
	if(!isInitialized)
		initialize();
	/*std::ifstream test("/home/javar/Desktop/Dropbox/Thesis/Programming/NLP2/fullIndex.txt", std::ifstream::binary);
	Json::Reader reader;
	bool parsingSuccess = reader.parse(test,jsonRoot,false);
	if(parsingSuccess)
		LOG("Success opening the index"<<endl);
	else
		LOG("Failed opening the index"<<endl);
	
	LOG("creating the Trie"<<endl);
	createWordTagsTrie();
	LOG("Finished creating the Trie"<<endl);
	createTransitionMatrixMap();
	createEmissionMatrixMap();
	createStartingMatrixMap();
	LOG("Presence of kumar = "<<!(isValidWord("writers") == NULL)<<endl);*/
}

TokenProcessor::~TokenProcessor(){
	LOG("Cleaning up the Trie"<<endl);
	clearTrie(root);
	LOG("Finished cleaning up the Trie"<<endl);
}

void TokenProcessor::initialize(){
	std::ifstream test("/home/javar/Desktop/Dropbox/Thesis/Programming/NLP2/fullIndex.txt", std::ifstream::binary);
	Json::Reader reader;
	bool parsingSuccess = reader.parse(test,jsonRoot,false);
	if(parsingSuccess)
		LOG("Success opening the index"<<endl);
	else
		LOG("Failed opening the index"<<endl);
	TokenProcessor::createWordTagsTrie();
	TokenProcessor::createTransitionMatrixMap();
	TokenProcessor::createEmissionMatrixMap();
	TokenProcessor::createStartingMatrixMap();
	isInitialized = true;
}

//Customized Tokenizer for Brown Corpus. Marks any non-alpha numeric character-!-hyphen as token delimiter.
//Adds the token delimter to the token list unless it is a space
void TokenProcessor::tokenize( const string & inSentence, vector<string> & outTokens ){
	char temp;
	string word;
	for(int i = 0;i< inSentence.size();i++){
		temp = inSentence[i];
		if( ( temp>= 48 && temp<=57 ) || ( temp>=65 && temp<= 90 ) || ( temp >=  97 && temp <= 122 ) ){
			word = word + inSentence[i];	
		}else {
			if( word.size() > 0 ){
				outTokens.push_back(word);
				word = "";
			}
			if( temp!=' '){
				outTokens.push_back(string(1,temp));
			}
		}
	}
	if( word.size() > 0 )
		outTokens.push_back(word);	
}

bool TokenProcessor::isNumber(const string & inWord){
	char temp;
	for(int i = 0; i<inWord.size();i++){
		temp = inWord[i];
		if( !( temp>= 48 && temp<=57 ) ){
			return false;	
		}
	}
	return true;
}

void TokenProcessor::createAllBitSequences(const string & inBitSequence, char ** outBitSequences, size_t count){
	int j = 0;
	int temp = count;
	LOG("Inside createAllBitSequences"<<endl);
	LOG("inBitSequence = "<<inBitSequence<<endl);
	for(int i = 0;i<inBitSequence.size();i++){
		LOG("i = "<<i<<endl);
		j = 0;
		if(inBitSequence[i] == '0' || inBitSequence[i] == '1'){
			while(j<pow(2,count)){
				outBitSequences[j][i] = inBitSequence[i];
				j++;
			}
			LOG("outside while loop"<<endl);
		} else if(inBitSequence[i] == '$'){
			char tempChar = '0';
			while(j<pow(2,count)){
				if( j!=0 && j%( (int)pow(2, temp-1) )  == 0){
					if(tempChar == '1')
						tempChar = '0';
					else if( tempChar == '0')
						tempChar = '1';
					else{
						LOG("Value of tempChar should be either 0 or 1. Exiting"<<endl);
						return;
					}
				}
				outBitSequences[j][i] = tempChar;
				j++;
			}
			temp--;
		} else{
			LOG("BitSequences should consist of either 0 or 1 or $. Either"<<endl);
			return;
		}
	}
	j = 0;
	while( j< pow(2, count) ){
		outBitSequences[j][inBitSequence.size()] = '\0';	
		j++;
	}
}

void TokenProcessor::createTokens(const string & inBitSequence, vector<string> & outBitSequences, vector< wordTagListBitSequence * > & outputVector){
	LOG("Nilesh"<<"Kumar"<<"Javar");
	int count = 0;
	int i_bs = 0;	
	while(i_bs < inBitSequence.size()){
		if(inBitSequence[i_bs] == '$')
			count++;
		i_bs++;
	}
	int size = inBitSequence.size();
	int tCount = pow(2,count);
	char ** bitSequences;
	bitSequences = new char *[tCount] ;
	for(int i = 0;i<tCount;i++){
		bitSequences[i] = new char[size+1];
	}
	createAllBitSequences(inBitSequence, bitSequences, count);
	//for(int i = 0;i<tCount;i++)
	//	LOG(bitSequences[i]<<endl);

	HuffmanCoding * hCode = HuffmanCoding::getInstance();
	string decodedStr("");
	wordVec tokensListVec;
	vecTagVec tagVecVec;
	const tagVec * tagVecPtr = NULL;	
	//tCount --> Number of possible bit sequences.
	int maxBitSequenceIndex = -1;
	double maxProb = 0;
	string tag("");
	vecTagVec tempTagVecVec; 
	tagVec tempTagVec;	
	bool isValidSequence = false;
	for(int i_bs = 0;i_bs < tCount; i_bs++){
		tempTagVecVec.clear();
		tokensListVec.clear();
		tempTagVec.clear();
		decodedStr ="";
		LOG("Decoding the sequence"<< bitSequences[i_bs]<<endl);
		hCode->decode( bitSequences[i_bs], decodedStr);
		LOG("Decoded as = "<<decodedStr<<endl);
		tokenize(decodedStr, tokensListVec);
		if( tokensListVec.size() > 0)
			isValidSequence = true;
		else
			isValidSequence = false;
		LOG("Valid Sequence : "<<isValidSequence<<endl);
		for( int i = 0;i < tokensListVec.size();i++){
			tagVecPtr = isValidWord(tokensListVec[i]);
			if( tagVecPtr == NULL ){
				LOG("Invalid bit sequence. Skipping it. Word not found = $"<<tokensListVec[i]<<"$"<<endl);
				isValidSequence = false;
				tagVecVec.clear();
				break;
			}
			else
				tempTagVecVec.push_back( *tagVecPtr);
		}
		if( !isValidSequence )
			continue;
		else{
			//This memory has to be deleted by calling fucntion.
			//Very bad choice of programming...But, who cares as I am out of time:P
			wordTagListBitSequence * tempPtr = new wordTagListBitSequence();
			tempPtr->words = tokensListVec;
			tempPtr->tagList =  tempTagVecVec; 
			tempPtr->bitSequence = bitSequences[i_bs];
			outputVector.push_back(tempPtr);
		}
	}			
	
	//Freeing the allocated memory
	for( int i = 0; i< tCount;i++){
		if(bitSequences[i]){
			delete [] bitSequences[i];
			bitSequences[i] = NULL;
		}
	}
	if( bitSequences ){	
		delete [] bitSequences;
		bitSequences = NULL;
	}
}

void TokenProcessor::createWordTagsTrie( ){
	const Json::Value wordToTagsMatrix = jsonRoot["wordToTags"];
	vector<string> tagList;
	vector<string> keys = wordToTagsMatrix.getMemberNames();
	LOG("Number of Words = "<<keys.size()<<endl);
	int hashValue = 0;
	TrieNode ** temp = &root;
	root = new TrieNode;
	root->tags = NULL;
	root->next = NULL;
	root->children = NULL;
	root->s = '\0';
	char keyChar;
	int matricesCount = 0;
	vector<string> tempTagVector;
	for(int i = 0;i<keys.size();i++){
		if( keys[i] == "Ordinary")
			LOG("Adding Ordinary"<<endl);
		tempTagVector.clear();
		tagList = wordToTagsMatrix[keys[i]].getMemberNames();
		temp = &root;
		for( int j = 0;j<keys[i].size();j++){
			keyChar = keys[i][j];
			if((*temp)->children){
				temp = &((*temp)->children);
				while( (*temp)->next && (*temp)->s!=keyChar)
					temp = &((*temp)->next);
				if( (*temp)->s == keyChar){
					continue;
				}
				else{
					(*temp)->next = new TrieNode;
					(*temp)->next->tags = NULL;
					(*temp)->next->next = NULL;
					(*temp)->next->children = NULL;
					(*temp)->next->s = keyChar;
					temp = &((*temp)->next);
					continue;
				}
			} else{
					(*temp)->children = new TrieNode;
					(*temp)->children->tags = NULL;
					(*temp)->children->next = NULL;
					(*temp)->children->children = NULL;
					(*temp)->children->s = keyChar;
					temp = &((*temp)->children);
					continue;
			}	
		}
		vector<string> * vecStrPtr = NULL;
		bool isDuplicate = false;
		if((*temp)->children != NULL){
			temp = &((*temp)->children);
			while((*temp)){
				/*if( (*temp)->s == '\0'){
					if( ((*temp)->tags).size() == 0 ){
						LOG("No Tags found for a word.Exiting"<<endl);
						while(1);
					}
					else{
						isDuplicate = true;
						break;
					}
				}*/			
				temp = &((*temp)->next);		
			}
		}else{
			temp = &((*temp)->children);
		}
		/*if( isDuplicate ){
			LOG("Duplciate found: Word = "<<inWord<<endl);
			continue;
		}*/
		(*temp) = new TrieNode;
		(*temp)->tags = new vector<string>;
		vecStrPtr = (*temp)->tags;
		(*temp)->next = NULL;
		(*temp)->children = NULL;
		(*temp)->s = '\0';
		const Json::Value tagsFromJson =  wordToTagsMatrix[keys[i]];
		tempTagVector = tagsFromJson.getMemberNames();	
		for(int k = 0;k<tempTagVector.size();k++)
			vecStrPtr->push_back(tempTagVector[k]);
		tempTagVector.clear();
	}
}

const vector<string> * TokenProcessor::isValidWord(const string & inWord){
	LOG("Finding Word = "<<inWord<<endl);
	if(root == NULL ){
		LOG("Root is NULL"<<endl);
		while(1);
	}

	TrieNode * temp = root;
	for(int i =0;i<inWord.size();i++){
		LOG(inWord[i]<<" ");
		if( temp &&  temp->children == NULL)
			return NULL;
		else{
			temp = temp->children;
			while(temp && temp->s!=inWord[i])
				temp = temp->next;
			if(temp && temp->s == inWord[i]){
				//LOG("Found it."<<endl);
				continue;
			} else {
				//LOG("Could not find"<<endl);
				return NULL;
			}					
		}
	}
	//LOG("value of char = "<<temp->s<<endl);
	temp = temp->children;
	while(temp && temp->s!='\0' && temp->tags==NULL){
		//LOG("Moving"<<endl);
		temp = temp->next;
	}
	//LOG("Here"<<endl);
	if(temp && temp->s == '\0' && temp->tags!=NULL){
		//LOG("Inside"<<endl);
		return (temp->tags);
	}
	else
		return NULL;
}

void TokenProcessor::clearTrie( TrieNode * root){
	if(root){
		if(root->next)
			clearTrie(root->next);
		if(root->children)
			clearTrie(root->children);
		if(root->tags){
			delete root->tags;
			root->tags = NULL;
		}
		delete root;
	}
	return;
}

void TokenProcessor::findWTSequence( wordVec & inWordVec, 
					vecTagVec & inVecTagVec,
					const string & inStartTag,
					const string & inEndTag,
					tagVec & inOutTagVec,
					int inOutLevel,
					double & inOutMaxProb){
	static int s_count = 0;
	int j = 0;	
	double wtSequenceProb = 0;
	if(inOutLevel == inVecTagVec.size() ){
		if( inWordVec.size() != inOutTagVec.size()){
			LOG("Something wrong happened"<<endl);
			while(1);
		} else{
			wtSequenceProb = findProbabilityWTSequence( inWordVec,
								    inStartTag, 
								    inEndTag, 
								    inOutTagVec);
			if(wtSequenceProb > inOutMaxProb){
				inOutMaxProb = wtSequenceProb;
			}
		}
		//LOG("count = "<<s_count++<<" "<<"Prob = "<<inOutMaxProb);
		//for(int i = 0; i< inOutTagVec.size();i++){
		//	LOG(inOutTagVec[i]<<" ");
		//}
		//LOG(endl);
		return;	
	}
	
	while( j < inVecTagVec[inOutLevel].size()){
		if( inOutLevel < inOutTagVec.size())
			inOutTagVec[inOutLevel] = inVecTagVec[inOutLevel][j];
		else
			inOutTagVec.push_back(inVecTagVec[inOutLevel][j]);
		findWTSequence(inWordVec,
				 inVecTagVec,
				 inStartTag,
				 inEndTag,	 
				 inOutTagVec, 
				 inOutLevel+1,
				 inOutMaxProb);
		j++;
	}

}

double TokenProcessor::findProbabilityWTSequence(wordVec & inWordVec, const string & inStartTag, const string & inEndTag, tagVec & inTagVec){
	//for(int i = 0;i< inWordVec.size();i++)
	//	LOG("word = "<<inWordVec[i]<<" tag = "<<inTagVec[i]<<endl);
	inTagVec.pop_back();
	inTagVec.push_back(inEndTag);
	if( inWordVec.size() != inTagVec.size()){
		LOG("WordVec and TagVec size does not match. Exiting"<<endl);
		while(1);
	}
	double tN, tD, eN, eD;	
	matrixMap::iterator mapItr;
	string prev_tag = inStartTag;
	double prob = 10;
	for(int i = 0;i<inWordVec.size();i++){
		//LOG("i = "<<i<<" word = "<<inWordVec[i]<<endl);
		tN = -1;
		tD = -1;
		eN = -1;
		eD = -1;
		if( i == 0 && inStartTag.size() == 0){
			mapItr = smMap.find(inStartTag);
			if( mapItr == smMap.end()){
				tN = 1;
			} else{
				tN = 1 + mapItr->second;
			}
			tD = smMap.size()-1 + smMap["total"];
		} else{
			mapItr = tmMap.find(prev_tag+"_"+inTagVec[i]);
			if( mapItr == tmMap.end())
				tN = 1;
			else
				tN = 1 + mapItr->second;
			if(  tmMap.find(prev_tag+"_total_count")!=tmMap.end() 
					&& tmMap.find(prev_tag+"_tags_count")!=tmMap.end() )
				tD = tmMap[prev_tag+"_total_count"]+tmMap[prev_tag+"_tags_count"];
			else{
				LOG("tag = "<<prev_tag+"_total_count"<<" : "<<(tmMap.find(prev_tag+"_total_count")==tmMap.end())<<"   "<<prev_tag+"_tags_count"<<" : "<<(tmMap.find(prev_tag+"_tags_count")==tmMap.end()));

				LOG(" 373 :: Tag could not be found. Exiting"<<endl);
				while(1);
			}	
		}
		mapItr = emMap.find(inTagVec[i]+"_"+inWordVec[i]);
		if(mapItr == emMap.end())
			eN = 1;
		else
			eN = emMap[inTagVec[i]+"_"+inWordVec[i]];
		if(  emMap.find(inTagVec[i]+"_total_wcount")!=tmMap.end() 
					&& emMap.find(inTagVec[i]+"_words_count")!=tmMap.end() )
				eD = emMap[inTagVec[i]+"_total_wcount"] + emMap[inTagVec[i]+"_words_count"];
		else{
			LOG("tag = "<<prev_tag+"_total_count"<<" : "<<(tmMap.find(prev_tag+"_total_count")==tmMap.end())<<"   "<<prev_tag+"_tags_count"<<" : "<<(tmMap.find(prev_tag+"_tags_count")==tmMap.end()));
			LOG("387 :: Tag could not be found. Exiting"<<endl);
			while(1);
		}	
		prev_tag = inTagVec[i];
		prob = prob * (tN/tD) *(eN/eD);
	}
	if(prob == 10)		
		return 0;
	else
		return prob/10;
}

void TokenProcessor::createTransitionMatrixMap(){
	Json::Value transitionMatrix = jsonRoot["transitionMatrix"];
	Json::Value tagListJson;	
	int count;
	vector<string> tags = transitionMatrix.getMemberNames();
	masterTagsList = tags;
	vector<string> associatedTagList;
	for(int i = 0;i< tags.size(); i++ ){
		LOG("tag = "<<tags[i]<<endl);
		tagListJson = transitionMatrix[tags[i]];	
		associatedTagList = tagListJson.getMemberNames();
		for(int j = 0; j < associatedTagList.size(); j++){
			count = tagListJson[associatedTagList[j]].asInt();
			if( associatedTagList[j] == "total"){
				if(tags[i] == "DT" || tags[i] == "CS")
					LOG("Adding the count of DT "<<count<<endl);
				tmMap[tags[i]+"_"+"total_count"] = count;
			}
			else{
				tmMap[tags[i]+"_"+associatedTagList[j]]	= count;
			}
		}
		if(tags[i] == "DT" || tags[i] == "CS")
			LOG("Adding tags associated = "<<associatedTagList.size()<<endl);
		tmMap[ tags[i]+"_tags_count"] = associatedTagList.size();
	}	
}

void TokenProcessor::createEmissionMatrixMap(){
	Json::Value emissionMatrix = jsonRoot["emissionMatrix"];
	Json::Value tagListJson;	
	int count;	
	vector<string> tags = emissionMatrix.getMemberNames();
	vector<string> associatedWordList;
	for(int i = 0;i< tags.size(); i++ ){
		tagListJson = emissionMatrix[tags[i]];	
		associatedWordList = tagListJson.getMemberNames();
		for(int j = 0; j < associatedWordList.size(); j++){
			count = tagListJson[associatedWordList[j]].asInt();
			if( associatedWordList[j] == "total")
				emMap[tags[i]+"_"+"total_wcount"] = count;
			else
				emMap[tags[i]+"_"+associatedWordList[j]] = count;
		}
		emMap[tags[i]+"_words_count"] = associatedWordList.size();
	}
}

void TokenProcessor::createStartingMatrixMap(){
	Json::Value startingMatrix = jsonRoot["startingMatrix"];
	Json::Value tagListJson;	
	int count;	
	vector<string> tags = startingMatrix.getMemberNames();
	for(int i = 0;i< tags.size(); i++ ){
		count = startingMatrix[tags[i]].asInt();	
		smMap[tags[i]] = count;
	}
	smMap["total"] = tags.size();
}

